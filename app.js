let parseString = require("xml2js-parser").parseString
let fs = require("fs-extra")

// So I remember: Player, Faction, Cards, Wining Side, Match Time

/**
* Converts a file from XML -> json
*
* @param {String} file - The XML file to read
* @returns {Promise} - A promise that resolves with jsonzzZ
*/
function convert(file) {
  return new Promise(function(resolve, reject) {
    fs.readFile(file).then(function(input) {
      parseString(input, async function(err, json) {
        let players = json.EventHistory.Players[0].Player
        let out = {}
        for(let player of players) {
          out[player.Identity[0].$.Name] = {
            name: player.Identity[0].$.Name,
            faction: player.FactionId[0],
            won: json.EventHistory.Results[0].WinningTeam[0] == player.FactionId[0] ? "Won" : "Lost",
            duration: json.EventHistory.MatchTime[0]
          }

          let units = player.Deck[0].Cards[0].Card

          for(let i in units) {
            out[player.Identity[0].$.Name][`unit${i}`] = units[i].$.Data
          }
          resolve(out)
        }
      })
    }).catch(reject)
  })
}

async function convertAll() {
  let filesIn = await fs.readdir("input")
  let files = filesIn.filter(function(file) {
    return file.endsWith(".xml")
  })
  for(let file of files) {
    let json = await convert(`input/${file}`)
    let csv = await convertCsv(json)
    fs.writeFile(`${__dirname}/output/${file}.csv`, csv)
    console.log(csv)
  }
}

async function convertCsv(json) {
  return `${Object.keys(json[Object.keys(json)[0]]).map(k => `"${k}"`).join(",")}
${Object.values(json).map(player => Object.values(player).map(k => `"${k}"`).join(",")).join("\n")}`
}

convertAll()
